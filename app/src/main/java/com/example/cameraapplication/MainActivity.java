package com.example.cameraapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    private static final int REQUEST_CODE_CAMERA = 0;
    private static final int REQUEST_CODE_GALLERY = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);
    }

    public void myClick(View view) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent,REQUEST_CODE_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap tempBitmap;
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK)
        {
            tempBitmap = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(tempBitmap);
        }
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK)
        {
            try {
                tempBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),data.getData());
                imageView.setImageBitmap(Bitmap.createScaledBitmap(tempBitmap,imageView.getWidth(),imageView.getHeight(),false));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void galleryClick(View view) {
        Intent galleryIntent = new Intent (Intent.ACTION_PICK);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,REQUEST_CODE_GALLERY);
    }
}